FROM ubuntu:latest
LABEL version="1"
LABEL description="TP Final"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y nginx
COPY default /etc/nginx/sites-available
COPY index.html /var/www/
RUN chown -R www-data:www-data /var/www
VOLUME ["/var/www","/etc/nginx"]
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
